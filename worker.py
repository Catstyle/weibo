#coding=utf-8
'''
Created on Mar 19, 2013

@author: liiikhai
'''

import os
from threading import Thread
import logging
import re
from urlparse import urljoin
import requests
from spider import proxies, headers, task_done
from settings import (
        base, pic_path, fans_limit,
        NAME_LEN, pic_per_folder,
    )

class Worker(Thread):
    
    def __init__(self, pool):
        
        super(Worker, self).__init__()
        self.daemon = True
        
        self.visited_urls = pool.visited_urls
        self.waitings = pool.waitings
        
        self.get_task = pool.task_queue.get
        self._task_done = pool.task_queue.task_done
        
        self._file_no = pool._get_file_no
        
        self.filter_list = pool.filter_list
        self._save_profile = pool.profile_queue.put
        
        self._init_requests()
    
    def _init_requests(self):
        """Requests configuration"""
        
        self.session = requests.session()
        self.session.headers = headers
        self.session.proxies=proxies
    
    def run(self):
        """main function in worker, handle the process of task"""
        
        _base = base
        get_request = self.session.get
        add_visited = self.visited_urls.add

        while 1:
            try:
                self.follow_link = follow_link = self.get_task()

                logging.info('got task, "%s"', follow_link)
                
                if follow_link == task_done:
                    break
                
                # tell other workers that this is handling/visited
                add_visited(follow_link)

                # strip javascript decoding!
                self.follow_link = follow_link = ''.join(follow_link.split("\\"))
                 
                # join the url, in case of relative url
                follow_link = urljoin(_base, follow_link)
                
                # start to get the people's data
                r = get_request(follow_link)
                
                # get and save one's profile
                self._get_profile(r.text)
                
                # now we continue with one's follows
                retry = 1
                while not self._parse_follows(r.text):
                    retry += 1
                    
                    if retry > 3:
                        break
                    
                    next_page = urljoin(_base, self.follow_link+'?page=%d' % retry)
                    logging.warn('retry %d times for prase follows for %s' % 
                                 (retry, next_page))

                    r = get_request(next_page)
            
            except KeyboardInterrupt:
                raise
            except Exception as e:
                logging.critical('thread exception...  %s' % e)
            finally:
                logging.info('completed task "%s"' % self.follow_link)
                self._task_done()
        
        logging.warn('worker stop')
    
    def _filter_info(self, info):
        filter_list=self.filter_list
        for kw in filter_list:
            if kw.strip() in info:
                return False

        return True
    
    def _get_profile(self, content,
                     re_info = re.compile(
                        r"""<span\s+class=\\"name\\">(.*?)<\\/span>.*?    # 0: name
                        <div\s+class=\\"pf_icon\s+clearfix\\">.*?
                        (?:<i\s+(.*?)><\\/i>|(?:node-type=\\"level\\")).*?# 1: approve if exist
                        class=\\"pf_intro.*?<span\s+class=\\"S_txt2\\".*?>(.*?)<\\/span>.*?    # introduce
                        <em\s+class=\\"W_ico12\s+(.*?)\\".*?>.*?    # gender
                        <em\s+class=\\"S_txt2\\"><a\s+href.*?title=\\"(.*?)\\">.*?    # address
                        <div\s+class=\\"pf_head_pic\\"\s+>.*?<img\s+src=\\"(.*?)\\"    # photo
                        """, re.X | re.M | re.U | re.DOTALL)):
        # one's info
        rv = re_info.search(content)
        
        if not rv:
            logging.warn('could not find profile for %s' % self.follow_link)
        else:
            logging.info('profile of %s: %s' %
                         (self.follow_link,
                          [ele and ele.strip('\\n\\t') for ele in rv.groups()]))
            profile = [self.follow_link]
            profile.extend(list(rv.groups()))
            
            # we only catch girls info...
            if profile[4] != 'female':
                logging.info('sorry, we only catch info for girls... %s is %s' % (self.follow_link, profile[4]))
            else:
                # save profile
                
                # use follow_link_url as the path to store img
                if not profile[2] or 'approve' not in profile[2]:
                    (self._get_photo(profile[0], profile[-1])
                     and self._save_profile(profile))
    
    def _get_photo(self, photo_path, photo_url, pic_path=pic_path):
        # we use user_url as the path to store img

        _no = self._file_no()
        img_path = pic_path % (_no / pic_per_folder, _no,
                '_'.join([''] + photo_path.split('/')[-2:]))
        if os.path.exists(img_path):
            logging.warn('%s already exist...' % img_path)
            return True

        default_photo = photo_url.split('\\/')[-2]
        if default_photo == '0' \
           or default_photo == 'face':
            logging.warn('oops...it is default photo, do not download it')
            return False

        photo_url = photo_url.replace("/180\\", "/50\\")
        r = self.session.get(''.join(photo_url.split("\\")))
        
        if 'image' not in r.headers['content-type']:
            logging.warn('%s returned response is not image content' % photo_path)
            return False
        
        with open(img_path, 'wb') as img:
            img.write(r.content)
        
        logging.info('found and got photo for %s, and stored as %s' % 
                     (self.follow_link, img_path))
        return True
    
    def _parse_follows(self, content,
                       re_follows=re.compile(
                        r"""<div\s+class=\\"name\\".*?
                        <a\s+.*?href=\\"\\.*?\\".*?>(.*?)<\\/a>    # 0: name 
                        [\\n\\t\s]+(?:<a\s+.*?class=\\"W_ico16\s+(.*?)\\"|  # 1: verification
                        <span.*?<em\s+class=\\"W_ico12\s+(.*?)\\"><\\/em>).*? # 2: gender
                        <div\s+class=\\"connect\\">.*?
                        href=\\"\\(.*?follow)\\".*?    # 3: follow link
                        \\/fans\\"\s+>(\d+)<\\/a> # 4: fans
                        """, re.X | re.M | re.U | re.DOTALL)):
        
        rv = re_follows.findall(content)

        # filter out the organizations
        rv = [ele[3] for ele in rv
              if len(ele[0]) <= NAME_LEN
              and self._filter_info(ele[0])
              and 'approve' not in ele[1]
              and 'female' == ele[2]
              and int(ele[4]) < fans_limit]

        # filter out already visited follow_links
        rv = [ele 
              for ele in rv
              if ele not in self.visited_urls]

        if not rv:
            return False
        self.waitings.update(rv)
        
        logging.info('found %d follow links in one\'s follow page for %s'
                     % (len(rv), self.follow_link))
        return True
