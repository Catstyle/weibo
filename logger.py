#coding=utf-8
'''
Created on Mar 22, 2013

@author: liiikhai
'''
from threading import Thread
import datetime
import time
from settings import target_num, base
try:
    from settings_dev import target_num, base
except ImportError:
    pass

class Logger(Thread):
    """A logger printing info to the stdout every 10 seconds"""
    
    def __init__(self, target):
        super(Logger, self).__init__()
        self.daemon = True
        
        self.target = target
        self.utcnow = datetime.datetime.utcnow
        self._log('Initiating logger for spider...')
    
    def run(self):
        log = self._log
        start_time = self.utcnow()
        
        # target info
        total = self.target.get_total
        target = target_num
        girls = self.target.get_girls
        
        self.running = True
        count = 0
        
        log('Logger start!')
        log('spider crawling "%s" for %d target_num' % (base, target))
        while self.running:
            # optimized
            time.sleep(2)
            count += 1
            if count < 5:
                continue
            else:
                count = 0
            
            log('')
            log('')
            log('###########Spider running info###########')
            log('girls: %d' % girls())
            log('target: %d' % target)
            log('total visit: %d' % total())
            log('elapsed time: %s' % (self.utcnow() - start_time))
        
        # finishing summary
        log('')
        log('')
        log('##############Spider summary##############')
        log('girls: %d' % girls())
        log('target: %d' % target)
        log('total visit: %d' % total())
        log('total elapsed time: %s' % (self.utcnow() - start_time))
    
    def stop(self):
        self.running = False
        self.endtime = self.utcnow()
    
    def _log(self, msg, formatter='[%s] %s'):
        print formatter % (self.utcnow(), msg)
