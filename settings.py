#coding=utf-8
'''
Created on Mar 15, 2013

@author: liiikhai
'''

import logging
import datetime

base = 'http://www.weibo.com/'
# your follow list url
my_follow = 'http://www.weibo.com/your_id_or_username/myfollow'

# !!!!you need to specified proxies that works!!!!
#proxies={"http": "http://10.32.235.41:8080",
#    "https": "http://10.32.235.41:8080"}
proxies = {}

#fill with your weibo.com cookie
cookie = ''
headers = {'Accept-Charset': 'gb2312,utf-8;q=0.7,*;q=0.3',
           'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
           'Accept-Encoding': 'gzip,deflate,sdch',
           'Accept-Language': 'en-US,en;q=0.8',
           'Connection': 'keep-alive',
           'User-Agent': 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.172 Safari/537.22',
           'cookie': cookie}

# identification of stopping a worker
task_done = '%#STOP#%'
# how many people do we want to catch
target_num = 100
# how many workers we will create
thread_num = 10

# we only catch those who has fans less than limit
fans_limit = 4000
NAME_LEN = 6

# profile saved in a sqlite3 dbfile
dbfile = 'weibo.db'
table_sql = '''CREATE TABLE IF NOT EXISTS
            weibo (id INTEGER PRIMARY KEY AUTOINCREMENT, 
            url nvarchar(1024) unique, 
            name nvarchar(128),
            introduce nvarchar(1024),
            gender nvarchar(7),
            address nvarchar(256),
            photo nvarchar(1024))'''
                
insert_sql = "INSERT INTO \
                weibo(url, name, introduce, gender, address, photo) \
                VALUES (?, ?, ?, ?, ?, ?);"

pic_per_folder = 3000
pic_folder = 'pic/%d'
pic_path = 'pic/%d/%d%s.jpg'
logfile = 'logs/weibo_%s.log' % datetime.datetime.utcnow().isoformat().replace(':', '-')[:-7]
logging_level = 5
levels={1: logging.CRITICAL,
        2: logging.ERROR,
        3: logging.WARN,
        4: logging.INFO,
        5: logging.DEBUG}

# logging configuration
def config_logfile(filename, level,
                   formatter='%(levelname)s\t- - [%(asctime)s] %(threadName)s:\t%(message)s',):
    
    try:
        logging.basicConfig(filename=filename,
                            format=formatter,
                            level=levels[level])
    except IOError as e:
        logging.error('failed to config logfile...\n%s' % e)
        raise
config_logfile(logfile, logging_level)
