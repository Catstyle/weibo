#coding=utf-8

import Queue
import requests
import re
import logging
from settings import (proxies, task_done, headers, my_follow,
                      dbfile, thread_num, table_sql, insert_sql,
                      target_num, pic_per_folder, pic_folder)
try:
    from settings_dev import *
except ImportException:
    pass
from worker import Worker
import sqlite3
import threading
#from threading import Lock

print my_follow

class Weibo(object):
    """A spider that getting users' introduces and avatars"""
    
    def __init__(self):
        
        # how many workers we will create, default 10
        self.thread_num = thread_num
        
        self.girls = 0

        import glob
        self.girls = sorted(
            # format 'pic/11/22_url.jpg'
            [int(jpg.split('/')[-1].split('_')[0])
             for jpg in glob.glob('pic/*/*.jpg')]
        )[-1]

        self._file_no = self.girls + 1
        if self.girls != 0:
            print 'recovering...'
            self._recover = True

        self._create_folders()
        self._init_requests()
        self._init_manager()
        self._init_filter()
        self._init_pool()
    
    def _create_folders(self):
        import os

        for idx in xrange(target_num/pic_per_folder + 2):
            if not os.path.exists(pic_folder % (idx,)):
                os.mkdir(pic_folder % (idx,))

    def _init_requests(self):
        """Requests configuration"""
        
        self.session = requests.session()
        self.session.headers = headers
        self.session.proxies=proxies
    
    def _init_pool(self):
        """init thread pool and start them"""
        
        logging.info('Initiating thread pool with size %d' % self.thread_num)
        
        self.pool = set()
        
        # file_no lock
        #self.file_no_lock = Lock()
        #self.acquire_file_no = self.file_no_lock.acquire
        #self.release_file_no = self.file_no_lock.release

        # communicating with thread
        self.task_queue = Queue.Queue()
        
        # result
        self.visited_urls = set()
        
        # tasks
        self.waitings = set()
        
        for _ in xrange(self.thread_num):
            worker = Worker(self)
            self.pool.add(worker)
            worker.start()
    
    def _init_db(self, dbfile):
        logging.info('Initiating db with "%s"' % dbfile)
        
        try:
            self.conn = sqlite3.connect(dbfile,
                                        isolation_level = None)
        except IOError as e:
            logging.critical('failed to init db...\n%s' % e)
            raise
        
        try:
            self.conn.execute(table_sql)
        except sqlite3.Error as e:
            self.conn.close()
            logging.critical('creating table failed...\n%s' % e)
            raise
    
    def _init_manager(self):
        """threads to handle data sent back from workers"""
        
        self.profile_queue = Queue.Queue()
        
        # start some threads handling data sent back
        self._profile_manager = threading.Thread(target=self._profile_manage)
        self._profile_manager.setDaemon(True)
    
    def _init_filter(self):
        self.filter_list = set()
        add = self.filter_list.add
        with open('filter.list', 'r') as fp:
            for line in fp.readlines():
                add(line.decode('utf-8').strip())

    def start(self):
        """main function"""
        
        logging.info('spider started')
        # start thread
        self._profile_manager.start()
        
        condition = self._recover \
                    and self._start_recovering() \
                    or self._start_from_my_follows()

        if condition:

            self.totals, target = 0, target_num
            assign_task = self.task_queue.put
            
            self._backup_file = open('follows_urls', 'a')
            running = 1
            while running:
                
                # get the tasks and clear duplicated data
                waitings = self.waitings.copy()
                self.waitings.clear()
                
                if not waitings:
                    logging.warn('no more waitings from last task')
                    break
                
                diff = target - self.girls
                logging.info('diff left: %d' % diff)
                for p in waitings:
                    assign_task(p)
                    self.totals += 1

                    diff -= 1
                    if diff <= 0:
                        self.task_queue.join()
                        if self.girls >= target:
                            running = 0
                            logging.info('target complete')
                            break
                        diff = target - self.girls
                        logging.info('diff left: %d' % diff)
                
                if running:
                    logging.info('waiting for tasks done')
                    self.task_queue.join()
                    logging.info('current tasks done')

                self._backup_urls()
        
        self._stop()
        logging.warn('weibo spider stopped...')
    
    def _stop(self):
        assign_task = self.task_queue.put
        pool_pop = self.pool.pop
        
        # stop workers
        for _ in xrange(self.thread_num):
            assign_task(task_done)
        
        for _ in xrange(self.thread_num):
            pool_pop().join()
        
        # stop data managers        
        self.profile_queue.put(task_done)
        self._profile_manager.join()
        
        self._backup_file.close()
    
    def _start_recovering(self):
        try:
            with open('follows_urls', 'r') as urls:
                cnt = 0
                for url in urls.readlines():
                    cnt += 1
                    if cnt <= self.girls:
                        continue
                    self.waitings.add(url.strip())
        except IOError:
            return False

        if not self.waitings:
            print 'not found urls in follows_urls needed to recovered...'
            return False

        print 'found %d urls in follows_urls needed to recovered...' %\
                len(self.waitings)
        return True

    def _start_from_my_follows(self):
        # first catch my profile
        r = self.session.get(my_follow)

        # start from my follows
        rv = re.findall(r'class=\\"info\\".*?href=\\"\\(.*?)\\"', r.content, re.M)
        logging.info('got %d follows in my follow info' % len(rv))
        
        if len(rv) == 0:
            logging.critical('Oops...last response url: %s' % r.url)
            return False
        
        self.waitings.update(rv)
        return True
    
    def _profile_manage(self, sql=insert_sql):
        """waiting for profile sent from workers
            and save it into sqlite3 db
        """
        # move to this thread to avoid thread check failure
        self._init_db(dbfile)
        
        get_profile = self.profile_queue.get
        profile_handled = self.profile_queue.task_done
        
        while 1:
            try:
                profile = get_profile()
                
                if profile == task_done:
                    break
                
                try:
                    with self.conn:
                        self.conn.execute(sql,
                                          (profile[0], profile[1], profile[3].strip('\\n\\t '),
                                           profile[4], profile[5], profile[6],))
                        self.girls += 1
                        logging.info('saved %s\'s profile into db file -- %s' %
                                     (profile[0], profile[1:]) )
                except sqlite3.IntegrityError:
                    logging.warn('%s\'s profile already saved in db' % profile[0])
                    self.girls += 1
                except sqlite3.Error as e:
                    logging.error('saving profile failed...%s' % e)
            finally:
                profile_handled()
        
        logging.warn('profile manager stopped...')
    
    def _backup_urls(self):
        write = self._backup_file.write
        for url in self.waitings:
            write(url + '\n')

    def _get_file_no(self):
        rv = self._file_no
        self._file_no += 1
        return rv

    def get_total(self):
        return len(self.visited_urls)
    
    def get_girls(self):
        return self.girls
