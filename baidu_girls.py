#coding=utf-8

from requests import get

base_url = 'http://touxiangaaatouxiang.duapp.com/nvsheng/%d.jpg'
img_path = 'baidu_girls/%d.jpg'

for idx in xrange(1, 697):
    print '%d out of 696' % (idx,)
    url = base_url % idx
    r = get(url)

    if 'image' not in r.headers['content-type']:
        print ('%s returned response is not image content' % url)
        continue
        
    with open(img_path % idx, 'wb') as img:
        img.write(r.content)

