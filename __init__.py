#coding=utf-8

#from gevent import monkey
#monkey.patch_all()

from spider import Weibo
from logger import Logger
import logging

def main():
    
    logging.info('')
    logging.info('')
    logging.info('start to crawl girls info from weibo...')

    w = Weibo()
    
    logger = Logger(w)
    logger.start()
    
    w.start()
    
    logger.stop()
    logger.join(10)

if __name__ == '__main__':
    main()
